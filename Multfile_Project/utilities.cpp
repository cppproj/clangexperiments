//============================================================================
// Name        : utilities.cpp
// Created on  : 26.02.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Utilities clang tests class 
//============================================================================

#include "utilities.h"

namespace Utilities {
	
	/** Some test function. **/
	void hello() {
		std::cout << "Hello!" << std::endl;
	}
	
}