//============================================================================
// Name        : utilities.h
// Created on  : 26.02.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Utilities clang tests class 
//============================================================================

#ifndef UTILITIES_INCLUDE_GUARD__H
#define UTILITIES_INCLUDE_GUARD__H

#include <iostream>

namespace Utilities {
	
	/** Some test function. **/
	void hello();
	
}
#endif // !UTILITIES_INCLUDE_GUARD__H
