
#include <iostream>
// #include <format>
#include <concepts>
#include <map>
#include <array>
#include <algorithm>
#include <thread>

class MyTestClass {
private:
   std::string name {"MyTestClass_Name_New"};

public:
   MyTestClass() {
      std::cout << __FUNCTION__ << std::endl;
   }

   void printInfo() const noexcept {
      std::cout << name << std::endl;
   }
};

inline std::map<std::string, int> get_map() {
   return std::map<std::string, int> {
      {"Key1", 1},
      {"Key2", 2},
      {"Key3", 3}
   };
}

/** Constexpr algoritms namespace: **/
namespace ConstexprAlgoritms {

   constexpr auto sort_constexpr(auto data)-> decltype(data) {
      std::sort(std::begin(data), std::end(data));
      return data;
   }

   void Test1() {
       constexpr auto sorted_array = sort_constexpr(std::array<int, 5>{5,4,3,2,1});
      // constexpr auto unsorted_array = std::array<int, 5>{5,4,3,2,1};

      static_assert(std::is_sorted(std::begin(sorted_array), std::end(sorted_array)),
                    "Input array is not sored.");
   }
};

namespace Multithreading {
    void foo() {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    void JThread_Simple_Test() {
        std::jthread t;
        std::cout << "before starting, joinable: " << std::boolalpha << t.joinable() << '\n';

        t = std::jthread(foo);
        std::cout << "after starting, joinable: " << t.joinable() << '\n';

        t.join();
        std::cout << "after joining, joinable: " << t.joinable() << '\n';
    }
}

int main(int argc, char** argv) {
   /*
   MyTestClass obj;
   std::cout << "Test34341" << std::endl;
   obj.printInfo();
   obj.printInfo();


   auto callable = [](const std::string& str) {
      std::cout << str << std::endl;
   };

   callable("Test");
   */

   Multithreading::JThread_Simple_Test();
}
